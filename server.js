

const express = require('express');
const socketio = require('socket.io');

var app = express();


let setupServer = () => {
    
  app.set('port', process.env.PORT || 30000);
  
  var server = app.listen(app.get('port'), () => {
        console.log("App is running at http://localhost:%s in %s mode", app.get('port'), app.get('env'));
        console.log('Press CTRL-C to stop\n');
    });

    var io = socketio(server);

    io.on('connect' ,(socket) => {
        console.log('SOCKET IO CONNECTED');
        socket.on('message',(input) => {
            console.log('RECEIVED::'+input);
            io.emit('message',`${input}_yo`);
        })

        socket.on('disconnect', () => {
            console.log('Client disconnected');
        });
    })

}

setupServer();

module.exports = app;